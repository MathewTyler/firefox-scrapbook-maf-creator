Firefox plugin ScrapBook X MAF creator is an open project based on ScrapBook MAF creator.

See also:

Documentation wiki / Downloads:
http://github.com/danny0838/firefox-scrapbook-maf-creator/wiki

Project repository:
http://github.com/danny0838/firefox-scrapbook-maf-creator

Original Firefox addon page:
http://addons.mozilla.org/firefox/addon/scrapbook-maf-creator/